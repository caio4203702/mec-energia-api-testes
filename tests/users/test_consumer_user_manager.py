import pytest
from django.contrib.auth import get_user_model
from users.models import CustomUser

User = get_user_model()

@pytest.mark.django_db
class TestCustomUserManager:
    def setup_method(self):
        self.user_manager = User.objects
        self.valid_user_data = {
            'email': 'testuser@example.com',
            'password': 'securepassword',
            'type': CustomUser.super_user_type,  # Ajustando para o tipo correto
        }
        self.valid_superuser_data = {
            'email': 'superuser@example.com',
            'password': 'supersecurepassword',
            'type': CustomUser.super_user_type,  # Ajustando para o tipo correto
        }
    
    def test_create_user_with_valid_data(self):
        user = self.user_manager.create(**self.valid_user_data)
        
        assert user.email == self.valid_user_data['email']
        assert user.check_password(self.valid_user_data['password']) is True
        assert user.type == self.valid_user_data['type']

    def test_create_user_without_email(self):
        with pytest.raises(ValueError, match='Email is required'):
            self.user_manager.create(email=None, password='securepassword')

    def test_create_user_without_password_in_production(self, settings):
        settings.ENVIRONMENT = 'production'
        self.valid_user_data['password'] = None
        user = self.user_manager.create(**self.valid_user_data)
        
        assert user.email == self.valid_user_data['email']
        assert not user.check_password(self.valid_user_data.get('password', ''))

    def test_create_user_with_invalid_user_type(self):
        invalid_user_data = self.valid_user_data.copy()
        invalid_user_data['type'] = 'invalid_type'
        
        with pytest.raises(Exception, match='Error Create User'):
            self.user_manager.create(**invalid_user_data)

    def test_create_superuser_with_valid_data(self):
        superuser = self.user_manager.create_superuser(**self.valid_superuser_data)
        
        assert superuser.email == self.valid_superuser_data['email']
        assert superuser.check_password(self.valid_superuser_data['password']) is True
        assert superuser.is_staff is True
        assert superuser.is_superuser is True
        assert superuser.type == CustomUser.super_user_type  # Ajustando para o tipo correto

    def test_create_superuser_without_is_staff(self):
        invalid_superuser_data = self.valid_superuser_data.copy()
        invalid_superuser_data['is_staff'] = False
        
        with pytest.raises(ValueError, match='Superuser must have is_staff=True'):
            self.user_manager.create_superuser(**invalid_superuser_data)

    def test_create_superuser_without_is_superuser(self):
        invalid_superuser_data = self.valid_superuser_data.copy()
        invalid_superuser_data['is_superuser'] = False
        
        with pytest.raises(ValueError, match='Superuser must have is_superuser=True'):
            self.user_manager.create_superuser(**invalid_superuser_data)
