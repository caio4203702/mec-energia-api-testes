import pytest

from universities.recommendation import Recommendation

@pytest.mark.django_db
def test_get_last_year_energy_bills_returns_12_bills():
     consumer_unit_id = 1  # exemplo de ID de unidade consumidora
     bills = Recommendation.get_last_year_energy_bills(consumer_unit_id)

     assert len(bills) == 12, "Deve retornar exatamente 12 contas de energia"

@pytest.mark.django_db
def test_get_last_year_energy_bills_identifies_pending_bills():
    consumer_unit_id = 1  
    bills = Recommendation.get_last_year_energy_bills(consumer_unit_id)
    
    for bill in bills:
        assert 'is_energy_bill_pending' in bill, "Cada conta deve indicar se está pendente"

@pytest.mark.django_db
def test_get_last_year_energy_bills_includes_bill_details():
    consumer_unit_id = 1  
    bills = Recommendation.get_last_year_energy_bills(consumer_unit_id)
    
    for bill in bills:
        assert 'energy_bill' in bill, "Os detalhes da conta de energia devem estar presentes em cada objeto"